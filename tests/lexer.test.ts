import { expect } from "chai"
import { createTokenBuilders, Token } from "../cjs/Tokens"
import { lex } from "../cjs/lexer"

describe("lexer", () => {
  const t = createTokenBuilders({ index: 0, text: "" })
  const stripPosition = ({ column, line, ...value }: Token) => value

  function expectLex(source: string, output: Token[]) {
    const actual = lex(source).map(stripPosition)
    expect(actual, `Lexing "${source}"`).to.deep.equal(output.map(stripPosition))
  }

  function expectNumber(source: string, num: number) {
    expectLex(source, [t.number(num, source)])
  }

  it("lexes single atoms", () => {
    expectLex("identifier", [t.identifier("identifier")])
    expectLex(`"string literal"`, [t.string("string literal")])
    expectLex("12345", [t.number(12345, "12345")])
    expectLex("'quote", [t.symbol("quote")])

    expectLex("#\\a", [t.char("a")])
    expectLex(';comments here', [t.comment('comments here')])
  })

  it("lexes booleans", () => {
    expectLex("#t", [t.boolean(true)])
    expectLex("#f", [t.boolean(false)])
  })

  describe("lexes numbers", () => {
    it("lexes vanilla numbers", () =>
      expectNumber("12345", 12345)
    )

    it("lexes octal literals", () =>
      expectNumber("#o777", 511)
    )
  })

  it("lexes two atoms", () => {
    expectLex("#t #f", [t.boolean(true), t.boolean(false)])
    expectLex("#t 1", [t.boolean(true), t.number(1)])
  })

  it("lexes lists", () => {
    expectLex("()", [t.left(), t.right()])
    expectLex("(a)", [t.left(), t.identifier("a"), t.right()])
    expectLex("(a 1)", [t.left(), t.identifier("a"), t.number(1), t.right()])
    expectLex("(a (b 1))", [t.left(), t.identifier("a"), t.left(), t.identifier("b"), t.number(1), t.right(), t.right()])
  })
})
