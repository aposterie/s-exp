import { expect } from "chai"
import { createTokenBuilders, Token } from "../cjs/Tokens"
import { parse, ParseError } from "../cjs/parser"
import { List, Identifier } from "../cjs/TreeNode"
import { read, json } from "../cjs";

describe("parser", () => {
  const t = createTokenBuilders({ index: 0, text: "" })
  const list = (...args) => List.of(...args)
  const left = t.left(), right = t.right()

  const expectParse = (...list: Token[]) => ({
    to: (results: any) =>
      expect(parse.sync(list, {}), results as any).to.deep.equal(results)
  })

  it("parses single atoms", () => {
    expectParse(t.identifier("word?")).to(Identifier.of("word?"))
    expectParse(t.string("string literal")).to("string literal")
    expectParse(t.number(12345)).to(12345)
    expectParse(t.symbol("quote")).to(Symbol.for("quote"))
    expectParse(t.char("#\\ ")).to(" ")
    expectParse(t.char("#\\a")).to("a")

    expectParse(t.comment('comments here')).to(null)
  })

  it("parses booleans", () => {
    expectParse(t.boolean(true)).to(true)
    expectParse(t.boolean(false)).to(false)
  })

  it("rejects multiple items outside a list", () => {
    expect(() => parse.sync([t.string("left"), t.string("right")])).to.throw(ParseError)
    expect(() => parse.sync([t.number(1), t.identifier("word?")])).to.throw(ParseError)
  })

  it("parses two atoms", () => {
    expectParse(left, t.string("string a"), t.string("string b"), right).to(["string a", "string b"])
  })

  it("parses lists", () => {
    expectParse(left, right).to(new List())
    expectParse(left, t.identifier("a"), right).to(list(Identifier.of("a")))
    expectParse(left, t.identifier("quux"), t.number(1), right).to(list(Identifier.of("quux"), 1))
    expectParse(left, t.identifier("foo"), left, t.identifier("bar"), t.number(1), right, right)
      .to(list(Identifier.of("foo"), list(Identifier.of("bar"), 1)))
  })

  it("parses object", () => {
    const r_ele = Symbol.for("r_ele")
    const entry = Symbol.for("entry")
    const re_restr = Symbol.for("re_restr")
    const ent_seq = Symbol.for("ent_seq")
    const pos = Symbol.for("pos")
    const reb = Symbol.for("reb")
    const keb = Symbol.for("keb")
    const sense = Symbol.for("sense")
    const k_ele = Symbol.for("k_ele")
    const gloss = Symbol.for("gloss")
    const misc = Symbol.for("misc")
    const lang = Symbol.for("lang")

    expect(read(`
      (entry
        (ent_seq "2840215")
        (k_ele
          (keb "ちび助"))
        (k_ele
          (keb "チビ助"))
        (r_ele
          (reb "ちびすけ")
          (re_restr "ちび助"))
        (r_ele
          (reb "チビすけ")
          (re_restr "チビ助"))
        (sense
          (pos "noun (common) (futsuumeishi)")
          (misc "word usually written using kana alone")
          (misc "derogatory")
          (gloss (lang "eng") "pipsqueak")
          (gloss (lang "eng") "little runt")
          (gloss (lang "eng") "midget")
          (gloss (lang "eng") "dwarf")))
    `, { ...json, Identifier: Symbol.for })).to.deep.equal(
      [
        entry,
        [ ent_seq, '2840215' ],
        [ k_ele, [ keb, "ちび助" ] ],
        [ k_ele, [ keb, "チビ助" ] ],
        [
          r_ele,
          [ reb, "ちびすけ" ],
          [ re_restr, "ちび助" ]
        ],
        [
          r_ele,
          [ reb, "チビすけ" ],
          [ re_restr, "チビ助" ]
        ],
        [
          sense,
          [ pos, "noun (common) (futsuumeishi)" ],
          [ misc, "word usually written using kana alone" ],
          [ misc, "derogatory" ],
          [ gloss, [lang, "eng"], "pipsqueak" ],
          [ gloss, [lang, "eng"], "little runt" ],
          [ gloss, [lang, "eng"], "midget" ],
          [ gloss, [lang, "eng"], "dwarf" ]
        ]
      ]
    )
  })
})
