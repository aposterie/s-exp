import { expect } from "chai"
import { Position } from "../cjs/Position"

describe("toPosition", () => {
  function expectPos(str: string, find: string, against: Position) {
    expect(Position({ text: str, index: str.indexOf(find) })).to.deep.equal(against)
  }

  it("should parse positions correctly", () => {
    const str = "abcdefg\nhijklmn"

    expectPos(str, "a", { line: 0, column: 0 })
    expectPos(str, "g", { line: 0, column: 6 })
    expectPos(str, "h", { line: 1, column: 0 })
  })

  it("should find \\n at the end of line", () => {
    const str = "abc\ndef"
    expectPos(str, "\n", { line: 0, column: 3 })
  })

  it("should throw if the index overflows", () => {
    expect(() => Position({ text: "abcef", index: 10 })).to.throw(RangeError)
  })
})