import { expect } from "chai"
import * as util from "../cjs/util"

describe("util", () => {
  it("has conforming array functions", () => {
    expect(util.head([1, 2, 3])).to.equal(1)
    expect(util.tail([1, 2, 3])).to.deep.equal([2, 3])
    expect(util.head("abc")).to.equal("a")
  })

  it("has conforming type predicates", () => {
    expect(util.isString("abc")).to.be.true
    expect(util.isString(0)).to.be.false

    expect(util.isFunction(Function)).to.be.true
    expect(util.isFunction("")).to.be.false

    expect(util.isNumber(0x1235)).to.be.true
    expect(util.isNumber(NaN)).to.be.true
    expect(util.isNumber("1234")).to.be.false
  })
})
