/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
export const noop = () => {}

export function assertType<T>(a: T) {}
export const identity = <T>(value: T) => value

export const prefix = (prefix: string) => (word: string) => prefix + word
export const head = <T = any>(list: ArrayLike<T>) => list[0]
export const tail = <T = any>(list: T[]) => list.slice(1)

export function isString(v: any): v is string {
  return typeof v === "string"
}
export function isNumber(v: any): v is number {
  return typeof v === "number"
}
export function isFunction(v: any): v is Function {
  return typeof v === "function"
}

export function testChar(char: string, test: string | RegExp) {
  return isString(test) ? char === test : test.test(char)
}

const nextTick = () => new Promise(resolve => setTimeout(resolve, 1))

export function createDualFunctions<R extends any[], T>(fn: (...args: R) => IterableIterator<T>) {
  return {
    sync(...args: R) {
      let res: IteratorResult<T>, it = fn(...args)
      while (!(res = it.next()).done);
      return res.value
    },
    async async(...args: R) {
      let res: IteratorResult<T>, it = fn(...args)
      while (!(res = it.next()).done) await nextTick()
      return res.value
    }
  }
}
