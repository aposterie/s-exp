/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
export interface Position {
  line: number
  column: number
}

/**
 * List must be in ascending order.
 */
function binarySearch<T>(list: T[], compare: (value: T) => 0 | 1 | -1, start = 0, end = list.length - 1) {
  const offset = Math.floor((end - start) / 2)
  const index = start + offset
  if (end < start) return -1
  switch (compare(list[index])) {
    case 0:
      return index
    case 1:
      return binarySearch(list, compare, index + 1, end)
    case -1:
      return binarySearch(list, compare, start, index - 1)
  }
}

let last = {
  text: "",
  index: 0,
  position: null as Position
}

export function Position({ text, index }: { text: string, index: number }) {
  if (index > text.length) {
    throw new RangeError("toPosition({ str { length }, int index }): index > length");
  }

  let pos: Position = { line: 0, column: 0 }
  let i = 0
  if (text === last.text && index >= last.index) {
    i = last.index
    pos = last.position
  }

  for (; i < index; i++) {
    if (text[i] === "\n") {
      pos.line++
      pos.column = 0
    } else {
      pos.column++
    }
  }
  last = { text, index: i, position: pos }
  return pos
}
