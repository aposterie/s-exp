/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
import { isString } from "./util"

const createOf = <R extends any[], T>(Class: { new (...args: R): T }) => (...args: R) => new Class(...args)

export class List<T = any> extends Array<T> {
  get name() {
    return this[0] instanceof Identifier ? this[0].toString()
      : isString(this[0]) ? this[0] : ""
  }
  get last() {
    return this[this.length - 1]
  }
  get tail() {
    return this.slice(1)
  }
  get isEmpty() {
    return this.length === 0
  }
  get(name: string) {
    return this.tail.find((item: any) => item && item.name === name)
  }
  getAll(name: string) {
    return this.tail.filter((item: any) => item && item.name === name)
  }
}

export class Identifier extends String {
  static of = createOf(Identifier)
  get name() {
    return this.toString()
  }
}

export function Char(value: string) {
  if (/^#\\[A-Z ]$/i.test(value)) {
    return value.slice(2)
  }
  return value
}
