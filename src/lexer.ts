/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
import { Position } from "./Position"
import { createTokenBuilders, Token } from "./Tokens"
import { testChar, isNumber } from "./util"

const whitespace = /^\s$/

type char = string & {}
type int = number & {}

export interface LexerState {
  readonly text: string
  readonly index: int
}

export function lex(text: string): Token[] {
  let index = 0

  const state: LexerState = {
    get text() { return text },
    get index() { return index },
  }

  const t = createTokenBuilders(state, { usePosition: false })

  ///// Inspection

  /** Get current index */
  const i = () => index
  /** Get character at index */
  const get = (index: int): char => text[index]
  /** Get current character */
  const ch = () => text[i()]
  /** Get next character */
  const peak = () => get(i() + 1)
  /** Get next `n` character */
  const peakN = (n: int) => text.slice(i(), i() + n)
  /** Get previous character */
  const last = () => get(i() - 1)
  /** Check if current character equals a string or matches RegExp */
  const is = (test: string | RegExp) => testChar(ch(), test)
  /** Return the position data of current character */
  const pos = () => Position(state)

  // Change State
  const move = (steps: int): int => index += steps

  /**
   * Consumes `n` characters and return them as a string.
   * Changes state.
   */
  const nextN = (n: number): string => (
    move(n),
    text.slice(i() - n, i())
  )

  const next = () => {
    const c = ch()
    move(1)
    return c
  }


  /** Assert the `i() + str.length` characters are equal to `str`.  */
  const expect = (str: string) =>
    invariant(peakN(str.length) === str, str)

  /** Consume and assert the `i() + str.length` characters are equal to `str`.  */
  function read(str: string) {
    invariant(nextN(str.length) === str, str)
    return str
  }

  function invariant(assertion: boolean, type: string) {
    if (!assertion) {
      const { line, column } = pos()
      throw SyntaxError(`Expected ${type} at line ${line}, column ${column}, got ${ch()}`)
    }
  }

  // parseInt/float that throws
  const parseInt: typeof Number.parseInt = (string, radix) => {
    const value = Number.parseInt(string, radix)
    invariant(isNumber(value) && !Number.isNaN(value), `a number of radix ${radix}`)
    return value
  }

  /**
   * Consume chars from current position until `condition()` returns true.
   * The character at which `condition()` returns true will *not* be included.
   */
  function readUntil(condition: () => boolean, cleanup?: () => void): string {
    let res = "", invoked = false
    while (ch() && !condition()) {
      invoked = true
      res += next()
    }
    if (invoked) move(-1)
    if (cleanup) cleanup()
    return res
  }
  readUntil.lineBreak = () => readUntil(() => is("\n"))
  readUntil.symbolEnd = () => readUntil(() => is(whitespace) || is(")"))

  function readString() {
    const quote = next()
    let res = "", c: char = ""
    while (c = ch()) {
      if (c === quote && last() !== "\\") {
        break
      } else {
        // This is the slowest line in the entire file after ch() function
        res += c
        next()
      }
    }
    return t.string(res)
  }

  function readDecimal() {
    const chars = readUntil.symbolEnd()
    const num = JSON.parse(chars)
    invariant(isNumber(num), 'a NumericLiteral')
    return t.number(num, chars)
  }

  function readOctal() {
    read("#o")
    const chars = readUntil.symbolEnd()
    return t.number(parseInt(chars, 8), `#o${chars}`)
  }

  function readSharp() {
    expect("#")
    // See page 136 of ANSI Standard Draft.
    switch (peak()) {
      case "\\":
        read("#\\")
        if (peak() === " ") return t.char(next()) // Special case: `#\ `
        return t.char(readUntil.symbolEnd())
      case "t":
        read("#t")
        return t.boolean(true)
      case "f":
        read("#f")
        return t.boolean(false)
      case "o":
        return readOctal()
      case " ":
      case ")":
      case "<":
        throw invariant(false, "valid sharp sign")
      default:
        return t.sharp()
    }
  }

  //// Lexington, New York
  const nodes: Token[] = []
  let c: char
  
  while (index < text.length && (c = ch())) {
    switch (c) {
      case "(":
        nodes.push(t.left())
        break

      case ")":
        nodes.push(t.right())
        break

      case "'":
        read("'")
        nodes.push(t.symbol(readUntil.symbolEnd()))
        break

      case "#":
        nodes.push(readSharp())
        break
        
      case ";":
        read(";")
        nodes.push(t.comment(readUntil.lineBreak()))
        break

      case "`":
        nodes.push(t.quote())
        break

      case " ":
      case "\t":
      case "\n":
        break

      case '"':
        nodes.push(readString())
        break

      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':  
      case '8':
      case '9':
        nodes.push(readDecimal())
        break

      default:
        nodes.push(t.identifier(readUntil.symbolEnd()))
        break
    }

    next()
  }

  return nodes
}
