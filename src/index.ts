/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
import { lex } from "./lexer"
import { Transform, parse } from "./parser"
import { prefix, head, isString } from "./util"

export { parse, lex }

export function read(text: string, options: Partial<Transform>) {
  return parse.async(lex(text), options)
}
export function readSync(text: string, options: Partial<Transform>) {
  return parse.sync(lex(text), options)
}

export const json: Partial<Transform> = {
  // List: Array.from,
  Identifier: String,
  Symbol: prefix("'"),
}

export const tree: Partial<Transform> = {
  Identifier: String,
  Symbol: prefix("'"),
  List(values) {
    const [first, ...rest] = values
    const names = rest.map(head)
    if (rest.length > 1 && rest.every(Array.isArray) && names.every(isString)) {
      const res = {}
      for (const [name, ...value] of rest) {
        res[name] = res[name] || []
        res[name].push(value)
      }
      console.log([first, res])
      return [first, res]
    }
    return values
  },
}