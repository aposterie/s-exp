/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
import { Position } from "./Position"

export type Token = {
  [key in keyof t]: ReturnType<t[key]>
}[keyof t]

export const enum NodeType {
  Symbol,
  String,
  Number,
  Char,
  Boolean,
  Identifier,
  Left,
  Right,
  Comment,
  Quote,
  Sharp,
}

type t = ReturnType<typeof createTokenBuilders>

interface StateProvider {
  readonly index: number
  readonly text: string
}

export function createTokenBuilders(state: StateProvider, { usePosition = true } = {}) {
  const node = <T extends NodeType, R>(type: T, rest?: R) => ({
    type,
    ...usePosition ? Position(state) : null,
    ...rest,
  })

  const left = node(NodeType.Left)
  const right = node(NodeType.Right)
  return {
    symbol: (description: string) => node(NodeType.Symbol, { description }),
    string: (value: string) => node(NodeType.String, { value }),
    number: (value: number, raw = String(value)) => node(NodeType.Number, { value, raw }),
    char: (identifier: string) => node(NodeType.Char, { identifier }),
    boolean: (value: boolean) => node(NodeType.Boolean, { value }),
    identifier: (name: string) => node(NodeType.Identifier, { name }),
    left: () => left,
    right: () => right,
    quote: () => node(NodeType.Quote),
    sharp: () => node(NodeType.Sharp),
    comment: (text: string) => node(NodeType.Comment, { text }),
  }
}
