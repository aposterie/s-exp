/**
 * Copyright (c) 2019. Tous droits réservés.
 * Disponible sous licence BSD-3-Clause.
 */
import { List, Identifier, Char } from "./TreeNode"
import { Token, NodeType } from "./Tokens"
import { identity, noop, createDualFunctions } from "./util"

// import { LinkedList } from "./LinkedList"
const LinkedList = List
type LinkedList<T = any> = List<T>

export interface Transform {
  Symbol(description: string): any
  String(value: string): any
  Number(value: number): any
  Char(identifier: string): any
  Identifier(atom: string): any
  Comment(text: string): any
  List(values: Iterable<any>): any
}

const defaultTransform: Transform = {
  Symbol: Symbol.for,
  String: String,
  Number: Number,
  Char: Char,
  List: identity,
  Identifier: Identifier.of,
  Comment: noop,
}

export class ParseError extends SyntaxError {}

const ASYNC_TICK = 27_500

function* parseNodes(nodes: Token[], transform: Partial<Transform> = {}) {
  const to = {...defaultTransform, ...transform}

  let index: number
  let current: LinkedList = null
  const stack = new LinkedList<LinkedList>()

  function pushStack() {
    const next = new LinkedList()
    if (current) {
      current.push(next)
      stack.push(current)
    } else {
      stack.push(next)
    }
    current = next
  }

  function popStack() {
    current = stack.pop()
    if (!current) {
      throw new ParseError("Unexpected closing parenthesis.")
    }
  }

  /**
   * Add a node to the current stack.
   * Also prevents adding two elements when outside a list.
   */
  function push(node?: any) {
    if (node === undefined) {
      // noop, however `null` is a valid node
    } else if (!current) {
      current = node
    } else if (current.length != null) {
      current.push(node)
    } else {
      throw new ParseError("Impossible to add another item: parent is not a list.")
    }
  }

  for (index = 0; index < nodes.length; index++) {
    const node = nodes[index]

    if (index % ASYNC_TICK === 0) yield

    switch (node.type) {
      case NodeType.Left:
        pushStack()
        break

      case NodeType.Right:
        to.List !== identity && current.forEach((item, i) => {
          if (item instanceof LinkedList)
            current[i] = to.List(item)
        })
        // to.List !== identity && current.forEachNode((item, i) => {
        //   if (item.value instanceof LinkedList)
        //     item.value = to.List(item.value)
        // })
        popStack()
        break

      case NodeType.Identifier:
        push(to.Identifier(node.name))
        break

      case NodeType.String:
        push(to.String(node.value))
        break

      case NodeType.Number:
        push(to.Number(node.value))
        break

      case NodeType.Char:
        push(to.Char(node.identifier))
        break

      case NodeType.Symbol:
        push(to.Symbol(node.description))
        break

      case NodeType.Boolean:
        push(node.value)
        break

      case NodeType.Comment:
        push(to.Comment(node.text))
        break

      default:
        // node.type
    }
  }

  if (stack.length) {
    throw new Error("A parenthesis is not closed.")
  }
  if (current instanceof LinkedList) {
    current = to.List(current)
  }

  return current
}

export const parse = createDualFunctions(parseNodes)
