class LinkedListNode<T> {
  constructor(public value: T) {}
  prev: LinkedListNode<T>
  next: LinkedListNode<T>
}

export class LinkedList<T = any> {
  head: LinkedListNode<T>
  tail: LinkedListNode<T>
  length: number

  [k: number]: never

  constructor() {
    this.head = null
    this.tail = null
    this.length = 0
  }

  push(value: T) {
    this.length++
    const next = new LinkedListNode(value)

    if (this.length === 1) {
      this.head = this.tail = next
    } else {
      this.tail.next = next
      next.prev = this.tail
      this.tail = next
    }
  }

  pop(): T {
    switch (this.length) {
      case 0:
        return

      case 1:
        this.length = 0
        const value = this.head.value
        this.head = this.tail = null
        return value

      default:
        this.length--
        const tail = this.tail
        tail.prev.next = null
        this.tail = tail.prev
        return tail.value
    }
  }

  private getNode(index: number) {
    if (index === this.length - 1) {
      return this.tail
    } else if (index >= this.length) {
      return
    }

    let node = this.head
    for (let i = 0; i < index; i++) {
      node = node.next
    }
    return node
  }

  get(index: number) {
    const node = this.getNode(index)
    return node && node.value
  }

  set(index: number, value: T) {
    const node = this.getNode(index)
    if (node) {
      node.value = value
    } else {
      throw RangeError(`OutOfBound for index ${index}`)
    }
  }

  *[Symbol.iterator]() {
    let node = this.head
    while (node) {
      yield node.value
      node = node.next
    }
  }

  forEachNode(fn: (value: LinkedListNode<T>, index: number) => void) {
    let i = 0
    let node = this.head
    while (node) {
      fn(node, i++)
      node = node.next
    }
    return this
  }

  forEach(fn: (value: T, index: number) => void) {
    let i = 0
    for (const value of this) {
      fn(value, i++)
    }
    return this
  }
}
