require("@babel/register")({
  "extensions": [".js", ".ts"],
  "presets": [
    "@babel/preset-typescript"
  ],
  "plugins": [
    "@babel/plugin-transform-modules-commonjs"
  ]
})