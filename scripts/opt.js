#!/usr/bin/env node -r esm
import * as fs from "fs-extra"
import { resolve, basename } from "path"
import * as babel from "@babel/core"

import inlineFunction from "../drafts/inline-pure"

const cjs = resolve(__dirname, "../cjs")
const files = (await fs.readdir(cjs)).map(file => resolve(cjs, file))

for (const file of files) {
  if (!file.endsWith(".js")) continue
  console.log(`Optimizing ${basename(file)}`)
  const res = await babel.transformFileAsync(file, {
    comments: false,
    plugins: [
      inlineFunction('ch', 'i', 'text', 'peak', 'last', 'pos'),
      "babel-plugin-minify-constant-folding",
      "babel-plugin-minify-dead-code-elimination",
    ]
  })

  await fs.writeFile(file, res.code)
}
